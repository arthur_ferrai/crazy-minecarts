Cart = {}
local mt = {__index = Cart}

local function clamp(min, max, val)
	return min < val and min or (max > val and max or val)
end

function Cart.new(x,y,world)
	local c = {}
	local leftWheelTouching, rightWheelTouching = false, false
	c.maxTorque = 25000
	c.maxSpeed = 700
	local wheelImg = love.graphics.newImage("img/cart wheel.png")
	local bodyImg = love.graphics.newImage("img/cart body.png")
	--set cart body
	c.b = love.physics.newBody(world,x,y, "dynamic")
	c.s1 = love.physics.newRectangleShape(-20,-15,5,30,math.pi*(-0.1))
	c.s2 = love.physics.newRectangleShape(0,0,30,5,math.pi*(0))
	c.s3 = love.physics.newRectangleShape(20,-15,5,30,math.pi*(0.1))
	c.bf1 = love.physics.newFixture(c.b,c.s1)
	c.bf2 = love.physics.newFixture(c.b,c.s2)
	c.bf3 = love.physics.newFixture(c.b,c.s3)
    c.bf1:setUserData("cart")
    c.bf2:setUserData("cart")
    c.bf3:setUserData("cart")
	
	-- set left wheel shape and fixture
	c.lw = love.physics.newBody(world,x-20,y+10, "dynamic")
	c.lwf = love.physics.newFixture(c.lw,love.physics.newCircleShape(0,0,10))
	c.lwf:setUserData("cart/leftWheel")
	c.lwf:setFriction(100000)
	c.lw:setMass(10)
	
    -- same for right wheel
	c.rw = love.physics.newBody(world,x+20,y+10, "dynamic")
	c.rwf = love.physics.newFixture(c.rw,love.physics.newCircleShape(0,0,10))
	c.rwf:setUserData("cart/rightWheel")
	c.rwf:setFriction(100000)
	c.rw:setMass(10)
	
	-- set joints
	c.lwj = love.physics.newRevoluteJoint(c.b,c.lw,x-20,y+10)
	c.rwj = love.physics.newRevoluteJoint(c.b,c.rw,x+20,y+10)

	c.setPhysicsCategory = function(cat)
		c.bf1:setCategory(cat)
		c.bf2:setCategory(cat)
		c.bf3:setCategory(cat)
	end

	c.setPhysicsMask = function(...)
		c.bf1:setMask(...)		
		c.bf2:setMask(...)		
		c.bf3:setMask(...)		
	end


	c.draw = function()
		local bodyX, bodyY = c.b:getWorldCenter()
		love.graphics.draw(bodyImg,bodyX,bodyY,c.b:getAngle(),1,1,bodyImg:getWidth()/2,bodyImg:getHeight()/2 + 5)
		local x1, y1 = c.lw:getPosition()
        local x2, y2 = c.rw:getPosition()
        love.graphics.draw(wheelImg,x1,y1,c.lw:getAngle(),1,1,wheelImg:getWidth()/2,wheelImg:getHeight()/2)
        love.graphics.draw(wheelImg,x2,y2,c.rw:getAngle(),1,1,wheelImg:getWidth()/2,wheelImg:getHeight()/2)
	end

	c.accelerate = function(amount)
		local sx, sy = c.b:getLinearVelocity()
		local speed = (sx^2 + sy^2)^0.5
		if speed >= c.maxSpeed then
			if (sx > 0 and amount > 0) or (sx < 0 and amount < 0) then amount = 0 end
		end
		c.lw:applyTorque(c.maxTorque * amount)
        c.rw:applyTorque(c.maxTorque * amount)
	end

	c.getSpeed = function()
		local sx, sy = c.b:getLinearVelocity()
        local speed = (sx^2 + sy^2)^0.5
	end

    c.getPosition = function()
        return c.b:getPosition()
    end

    -- since this function is used to reset car position, it needs to 
    -- reset every physic attribute related to dynamics
    c.setPosition = function(x,y)
        c.b:setPosition(x,y)
    	c.b:setAngle(0)
    	c.b:setLinearVelocity(0,0)
        c.b:setAngularVelocity(0)
        c.lw:setPosition(x-20,y+10)
        c.rw:setPosition(x+20,y+10)
        c.lw:setLinearVelocity(0,0)
        c.lw:setAngularVelocity(0)
        c.rw:setLinearVelocity(0,0)
    	c.rw:setAngularVelocity(0)
    end
    setmetatable(c,mt)
    return c
end

setmetatable(Cart,{__call = function(_,...) return Cart.new(...) end})