Rail = {}
local mt = {__index = Rail}

function Rail.Load(pointsLists, world,w,h)
    local r = {}
    local curvesList = {}
    local shapes = {}
    local fixtures = {}
    local tieDistance = 50
    local tieImage = love.graphics.newImage("img/rail wood.png")
    local tiePositions = {}

    local body = love.physics.newBody(world,0,0,"static")
    
    for _, points in pairs(pointsLists) do
        local c = love.math.newBezierCurve(points)
        table.insert(curvesList,c:render())
        local s = love.physics.newChainShape(false,unpack(c:render()))
        table.insert(shapes,s)
        local f = love.physics.newFixture(body,s)
        f:setUserData("Rail")
        table.insert(fixtures,f)
    end

    r.setPhysicsCategory = function(cat)
        for _,fixture in ipairs(fixtures) do
            fixture:setCategory(cat)
        end
    end

    r.setPhysicsMask = function(...)
        local c, m, g

        for _,fixture in ipairs(fixtures) do
            fixture:setMask(...)
        end
    end
    for _, curve in ipairs(curvesList) do
		-- draw rail details
		local lastPoint = 1
		for i = 1, #curve, 2 do
			if ((curve[i] - curve[lastPoint])^2 + (curve[i + 1] - curve[lastPoint + 1])^2) >= tieDistance*tieDistance then
                table.insert(tiePositions,{x=curve[i],y=curve[i+1],r=math.atan((curve[i + 1] - curve[i - 9])/(curve[i] - curve[i - 10]))})
				lastPoint = i
			end
		end
	end
	
    local tieOffsetX, tieOffsetY = tieImage:getWidth()/2,tieImage:getHeight()/2
	r.draw = function()
        local r,g,b,a = love.graphics.getColor()
        local s = love.graphics.getLineWidth()
		for _, curve in ipairs(curvesList) do
            love.graphics.setColor(r/2,g/2,b/2,255)
            love.graphics.setLineWidth(15)
            love.graphics.line(curve)
            love.graphics.setColor(0,0,0,255)
            love.graphics.setLineWidth(5)
            love.graphics.line(curve)
        end
        love.graphics.setColor(r,g,b,a)
        love.graphics.setLineWidth(s)

        for _,v in ipairs(tiePositions) do
            love.graphics.draw(tieImage,v.x,v.y,v.r,1,1,tieOffsetX,tieOffsetY)
        end
	end

    return r
end