-- simple linear interpolation
-- cur = current value
-- dest = destination value
-- amount = percent of approx. to dest (clamped into 0~1)
function Lerp (cur,dest,amount)
    -- clamp amount
    amount = amount < 0 and 0 or (amount > 1 and 1 or amount)
    return (cur == dest) and dest or (cur + (dest - cur) * amount)
end

-- square lerp
function SquareLerp (cur,dest,amount)
    return Lerp(cur,dest,amount*amount)
end

-- sine lerp
function SineLerp (cur,dest,amount)
    return Lerp(cur,dest,math.sin(amount-0,5)*math.pi/2+0.5)
end