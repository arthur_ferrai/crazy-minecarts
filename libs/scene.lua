--[[
A very basic scene manager
All it does is load a .lua from "scenes" folder, and call 
its functions (via main code, using a modified love.run).

A scene file must return a table, which has the love 
callbacks (not necessarily all of them).

To change the current scene, you just need to call 
Scene.Load(name), where "name" is the .lua file name of 
the scene you want to load. It then calls unload function 
(if any), changes current scene and call the load function
of the new scene. Quick and dirty. :)
]]

local currentScene

Scene = {}
local mt = {__index = function(t,k)
                if currentScene and type(currentScene[k]) == "function" then
                    return currentScene[k]
                end
                return function( ... ) end
            end}

setmetatable(Scene,mt)

function Scene.Load(name)
    if currentScene then
        Scene.unload()
    end
    local chunk = love.filesystem.load("scenes/"..name..".lua")
    if not chunk then
        error("Attempt to load scene \"" .. name .. "\", but was not found in \"scenes\" folder.", 2)
    end
    currentScene = chunk()
    collectgarbage()
    Scene.load()
end