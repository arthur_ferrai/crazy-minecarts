--[[
Camera utility module

grabbed from http://nova-fusion.com/2011/04/19/cameras-in-love2d-part-1-the-basics/

used to make an easy camera system

Use example:
============================================================
require "camera"

local cam = Camera()

function love.draw()
	cam:set()
	-- all drawings under this camera go here
	cam:unset()
end
============================================================
]]

local function clamp (val,min,max)
	return val < min and min or (val > max and max or val)
end

Camera = {}
Camera.__index = Camera

--[[
Creates a new camera
_x  = camera X pos
_y  = camera Y pos
_sx = camera scale in X
_sy = camera scale in Y
_r  = camera rotation degrees
]]
function Camera.new(x,y,sx,sy,r)
	x = x or 0
	y = y or 0
	sx = sx or 1
	sy = sy or sx
	r = r or 0
	local layers = {}
	local bounds = nil
	local shader = nil
	local canvas = love.graphics.newCanvas()
	local c = {
	}

	local function set ()
		love.graphics.push()
		love.graphics.rotate(-r)
		love.graphics.scale(1 / sx, 1 / sy)
		love.graphics.translate(-x, -y)	
	end

	local function unset()
		love.graphics.pop()
	end

	c.draw = function()
		canvas:clear()
		love.graphics.setCanvas(canvas)
		local bx, by = x, y
		for _,v in ipairs(layers) do
			x = math.floor(bx*v.scale)
			y = math.floor(by*v.scale)
			set()
			v.draw()
			unset()
		end
		-- restore camera offset
		x,y = bx, by
		love.graphics.setCanvas()
		if shader then love.graphics.setShader(shader) end
		love.graphics.draw(canvas)
		love.graphics.setShader()
	end

	
	c.setShader = function(s)
		if s then 
			shader = s
		else
			shader = nil
		end
	end

	c.setX = function(_x)
		if bounds then
			x = math.floor(clamp(_x, bounds.x1, bounds.x2))
		else
			x = math.floor(_x)
		end	
	end

	c.setY = function(_y)
		if bounds then
			y = math.floor(clamp(_y, bounds.y1, bounds.y2))
		else
			y = math.floor(_y)
		end	
	end

	c.getX = function() return x end
	c.getY = function() return y end

	c.getPosition = function() return x, y end	

	c.getHeight = function()
		return love.graphics.getHeight()
	end

	c.getWidth = function()
		return love.graphics.getWidth()
	end

	c.move = function(dx, dy)
		c.setX(x + (dx or 0))
		c.setY(y + (dy or 0))
	end

	c.rotate = function(dr)
		r = r + (dr or 0)
	end	

	c.scale = function(_sx, _sy)
		_sx = _sx or 1
		sx = sx * _sx
		sy = sy * (_sy or _sx)
	end

	c.setPosition = function(_x, _y)
		c.setX(_x)
		c.setY(_y)
	end

	c.setScale = function(_sx,_sy)
		sx = _sx or sx
		sy = _sy or sy
	end

	c.setBounds = function(x1, y1, x2, y2)
		if not x1 then bounds = nil return end
		bounds = {x1 = x1, 
				  y1 = y1, 
				  x2 = x2, 
				  y2 = y2}
	end

	c.getBounds = function()
		return unpack(bounds)
	end

	c.newLayer = function(scale,func)
		table.insert(layers,{draw = func, scale = scale})
		table.sort(layers, function(a,b) 
			-- 0 layer is on top
			if a.scale == 0 then return false end
			if b.scale == 0 then return true end
			return a.scale < b.scale
		end)
	end

	c.mousePosition = function()
		return love.mouse.getX() * sx + x, love.mouse.getY() * sy + y
	end

	setmetatable(c,Camera)
	return c
end
setmetatable(Camera, {__call = function(_,...) return Camera.new(...) end})
