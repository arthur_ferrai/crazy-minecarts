--[[
CoroutineRunner: call and resume coroutines automatically at each frame update
]]

coroutineRunner = {}

local functions = {}

-- add a function to coroutine list
function coroutineRunner.add(func)
	if type(func) ~= "function" then error("Error adding coroutine: not a function.",2) end
	local co = coroutine.create(func)
	table.insert(functions,co)
	return co
end

local cs = coroutine.status
local cr = coroutine.resume
function coroutineRunner.step(steps)
	steps = steps or 1
	for i=1,steps do
		local j=1
		while j <= #functions do
			if cs(functions[j]) ~= "dead" then
				cr(functions[j])
				j = j + 1
			else
				table.remove(functions,j)
			end
		end
	end
end

local t = love and love.timer.getTime or os.time
function yieldWait(time)
	local startTime = t()
	repeat
		coroutine.yield()
	until not (t() - startTime < time)
end