require "lerp"
-- fade screen from/to black
Fader = {}

local currentFade
local fadeDir = 1
local fadeSpeed = 0

function Fader.init()
	currentFade = 0
	fadeDir = 1
end

function Fader.fadeIn(speed)
	fadeDir = 1
	fadeSpeed = speed or 1
end

function Fader.fadeOut(speed)
	fadeDir = 0
	fadeSpeed = speed or 1
end

function Fader.update(dt)
	currentFade = Lerp(currentFade,fadeDir,dt*fadeSpeed)
end

function Fader.draw()
	local _,_,_,a = love.graphics.getColor
	love.graphics.setColor(currentFade*255,currentFade*255,currentFade*255,a)
end

function Fader.isFading()
	return math.floor(currentFade*10) ~= fadeDir*10
end