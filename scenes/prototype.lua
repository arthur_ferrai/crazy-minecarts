require "libs.scene"
require "cart"
require "rail"
require "libs.camera"
require "item"
require "libs.coroutineRunner"
local tween = require "libs.tween"
local st = require "gamestatus"

local s = {}

local levelData = {}
local itemList = {}
local respawnPoint = {}
local shader
local cart
local world
local isPaused
local speedMult = 1

local pixelChangeTime = 0.05
local freeCam = false
local showDebug = false

local collisionGroups = {
    BOUNDARY = 0x0001,
    RAIL     = 0x0002,
    CART     = 0x0004,
    CHAR     = 0x0008,
    ITEM     = 0x0010,
}

-- make the item action, called when cart collides with item
local function doItemAction(name,x,y)
    local switch = {
        turbo      = function() cart.maxSpeed = cart.maxSpeed * 1.5 speedMult = speedMult + 0.5 end,
        slow       = function() cart.maxSpeed = cart.maxSpeed * 0.5 speedMult = speedMult - 0.5 end,
        ore        = function() st.score = st.score + 100 end,
        checkpoint = function() respawnPoint = {x, y} end
    }

    if switch[name] then switch[name]() end
end

local function fadeOutAndExit()
    local factor = 4096
    while factor > 1 do -- moar pixelz
        yieldWait(pixelChangeTime)
        factor = math.ceil(factor/2)
        shader:send("factor",factor)
    end
    Scene.Load("levelTransition")
end

local function passLevel()
    world:setCallbacks(nil)
    st.currentLevel = st.currentLevel+1
    coroutineRunner.add(fadeOutAndExit)
end

-- physics callback
local function beginContact(f1,f2,c)
    local ud1, ud2 = f1:getUserData(), f2:getUserData()
    -- doesn't have userdata = ignore
    if not ud1 or not ud2 then return end
    -- filter rail collisions since we don't need to deal with them
    if ud1 == "Rail" then return end
    if ud2 == "Rail" then return end

    -- If we got an item, remove it from items list (and of course do the item action)
    if ud1:sub(1,4) == "item" then
        for i,v in ipairs(itemList) do
            if v.getFixture() == f1 then
                table.remove(itemList,i)
                doItemAction(string.sub(ud1,6),v.getPos())
                f1:destroy()
                return
            end
        end
    end
    if ud2:sub(1,4) == "item" then
        for i,v in ipairs(itemList) do
            if v.getFixture() == f2 then
                table.remove(itemList,i)
                doItemAction(string.sub(ud2,6),v.getPos())
                f2:destroy()
                return
            end
        end
    end
    if ud1:sub(1,5) == "limit" then
        if ud1:sub(7) == "down" then
            coroutineRunner.add(function()
                cart.setPosition(unpack(respawnPoint))
                ball.b:setPosition(respawnPoint[1],respawnPoint[2]-10)
            end)
            return
        end
        if ud1:sub(7) == "right" and ud2:find("cart") then
            f1:destroy() -- need to do this to prevent continuous collision detection
            passLevel()
            return
        end
    end
end

local factor = 1

function s.load()
    isPaused = false
    levelData = love.filesystem.load("rails/"..st.levelOrder[st.currentLevel]..".lua")()
    cam = Camera()
    factor = 1
    shader = love.graphics.newShader "shaders/pixelize.glsl"
    shader:send("factor",factor)
    cam.setShader(shader)
    cam.setBounds(levelData.limits.l,levelData.limits.u,levelData.limits.r-cam.getWidth(),levelData.limits.d-cam.getHeight())
    
    -- game layer
    cam.newLayer(1,function()
        -- make paused screen darker
        if isPaused then
            love.graphics.setColor(128,128,128,255)
        else
            love.graphics.setColor(255,255,255,255)
        end
        local x,y = ball.b:getPosition()
        love.graphics.circle("fill",x,y,10)
        cart.draw()
        floor.draw()

        for _,item in ipairs(itemList) do
            item.draw()
        end
        if showDebug then
            debugWorldDraw(world)
        end
    end)
    
    cam.newLayer(0,function()
        love.graphics.print(st.score)
        -- show "PAUSED!" on top of everything
        if isPaused then
           love.graphics.setColor(255,255,255,255)
           love.graphics.printf("PAUSED!",0,love.graphics.getHeight()*0.4,love.graphics.getWidth()/10,"center",0,10) 
        end        
    end)
    
    world = love.physics.newWorld(0, 600, true)
    world:setCallbacks(beginContact)
    
    --create world limits
    worldLimits = {}
    worldLimits.b = love.physics.newBody(world,0,0)
    worldLimits.fl = love.physics.newFixture(worldLimits.b,love.physics.newPolygonShape(levelData.limits.l - 10, levelData.limits.u,
                                                                                        levelData.limits.l, levelData.limits.u,
                                                                                        levelData.limits.l, levelData.limits.d,
                                                                                        levelData.limits.l - 10,levelData.limits.d))
    --worldLimits.fl:setSensor(true)
    worldLimits.fl:setUserData("limit/left")
    
    worldLimits.fr = love.physics.newFixture(worldLimits.b,love.physics.newPolygonShape(levelData.limits.r, levelData.limits.u,
                                                                                        levelData.limits.r + 10, levelData.limits.u,
                                                                                        levelData.limits.r + 10, levelData.limits.d,
                                                                                        levelData.limits.r, levelData.limits.d))
    worldLimits.fr:setSensor(true)
    worldLimits.fr:setUserData("limit/right")
    
    worldLimits.fu = love.physics.newFixture(worldLimits.b,love.physics.newPolygonShape(levelData.limits.l, levelData.limits.u - 10,
                                                                                        levelData.limits.r, levelData.limits.u - 10,
                                                                                        levelData.limits.r, levelData.limits.u,
                                                                                        levelData.limits.l, levelData.limits.u))
    --worldLimits.fu:setSensor(true)
    worldLimits.fu:setUserData("limit/up")
    
    worldLimits.fd = love.physics.newFixture(worldLimits.b,love.physics.newPolygonShape(levelData.limits.l, levelData.limits.d,
                                                                                        levelData.limits.r, levelData.limits.d,
                                                                                        levelData.limits.r, levelData.limits.d + 10,
                                                                                        levelData.limits.l, levelData.limits.d + 10))
    worldLimits.fd:setSensor(true)
    worldLimits.fd:setUserData("limit/down")

    -- create items
    for k,v in ipairs(levelData.items) do
        if v.name == "checkpoint" and #respawnPoint == 0 then
            respawnPoint = {v.x, v.y}
        end
        local i = Item.new(world,v.name,v.x,v.y)
        i.setPhysicsCategory(collisionGroups.ITEM)
        i.setPhysicsMask(collisionGroups.RAIL,collisionGroups.CHAR)
        table.insert(itemList,i)
    end

    --create cart
    cart = Cart(respawnPoint[1],respawnPoint[2],world)
    cart.setPhysicsCategory(collisionGroups.CART)
    cart.setPhysicsMask(collisionGroups.CART)

    cam.setPosition(respawnPoint[1]-cam.getWidth()/2, respawnPoint[2]-cam.getHeight()/2)

    floor = Rail.Load(levelData.pointsGroups,world,levelData.limits.r - levelData.limits.l,  levelData.limits.d - levelData.limits.u)
    floor.setPhysicsCategory(collisionGroups.RAIL)
    floor.setPhysicsMask(collisionGroups.RAIL, collisionGroups.CHAR)

    
    ball = {}
    ball.b = love.physics.newBody(world,respawnPoint[1],respawnPoint[2]-10,"dynamic")
    ball.b:setBullet(true)
    ball.f = love.physics.newFixture(ball.b,love.physics.newCircleShape(0,0,10))
    ball.f:setCategory(collisionGroups.CHAR)
    local _, _, _g = ball.f:getFilterData()
    ball.f:setMask(collisionGroups.RAIL,collisionGroups.ITEM)
    ball.f:setUserData("Char")

    coroutineRunner.add(function ()
        local factor = 1
        while factor < 4096 do
            factor = factor * 2
            yieldWait(pixelChangeTime)
            shader:send("factor",factor)
        end
    end)
    isPaused = false
end


function s.unload()
    cam = nil
    cart = nil
    floor = nil
    ball = nil
    levelData = {}
    world:destroy()
    world = nil
    love.graphics.setColor(255,255,255,255)
end

function s.keypressed(key)
    if key == "escape" then Scene.Load("menu") end
    if key == "f1" then showDebug = not showDebug end
    if key == "p" or key == "P" then isPaused = not isPaused end
end

function s.update(dt)
    if isPaused then 
        -- TODO: does pause screen needs update?
        return 
    end
    coroutineRunner.step()
    if not world then return end
    world:update(dt)
    if isMobile then
        for index = 1, love.touch.getTouchCount() do
            local _, x = love.touch.getTouch(index)
            if x <= 0.1 then
                cart.accelerate(-speedMult)
            elseif x >= 0.9 then
                cart.accelerate(speedMult)
            end
        end
    else
        if love.keyboard.isDown("left") then
            cart.accelerate(-1*speedMult)
        elseif love.keyboard.isDown("right") then
            cart.accelerate(1*speedMult)
        end
    end
    if not freeCam then
        --cam.setBounds(levelData.limits.l,levelData.limits.u,levelData.limits.r-cam.getWidth(),levelData.limits.d-cam.getHeight())
        local x, y = cart.getPosition()
        local camX, camY = cam.getPosition()
        camX = Lerp(camX,x - cam.getWidth()/2,dt*5)
        camY = Lerp(camY,y - cam.getHeight()/2,dt*5)
        cam.setPosition(camX,camY)
    else
        cam.setBounds()
        local x,y
        if love.keyboard.isDown("a") then
            x = -10
        end
        if love.keyboard.isDown("d") then
            x = 10
        end
        if love.keyboard.isDown("w") then
            y = -10
        end
        if love.keyboard.isDown("s") then
            y = 10
        end
        cam.move(x,y)
    end
end

function s.draw()
    cam.draw()
end

return s