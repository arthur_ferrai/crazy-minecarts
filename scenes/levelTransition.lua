require "libs.scene"
require "fader"

local st = require "gamestatus"

local s = {}

local timeout = 5
local currentTime

function s.load()
    currentTime = 0
    currentFade = 0
    fadeDir = 1
    exiting = false
    Fader.init()
    Fader.fadeIn(5)
end

function s.unload()
	love.graphics.setColor(255,255,255,255)
end

function s.draw()
	--love.graphics.setColor(255*currentFade,255*currentFade,255*currentFade,255)
	Fader.draw()
    love.graphics.print("Next level: " .. (st.levelOrder[st.currentLevel] or "") .. ".",love.graphics.getWidth()/2,love.graphics.getHeight()/2)
end

function s.update(dt)
	Fader.update(dt)
	currentTime = currentTime + dt
	if currentTime >= timeout then
		Fader.fadeOut(5)
    	exiting = true
    end
    if exiting and not Fader.isFading() then
        if st.levelOrder[st.currentLevel] then
            Scene.Load("prototype")
        else
            Scene.Load("menu")
        end
    end
end

return s