require "libs.scene"
local tween = require "libs.tween"

local s = {}

local titleSprite = {}
local touchText = {}
local playButton = {}
local editorButton = {}
local quitButton = {}

function s.load()
	titleSprite = {
		text = "CRAZY\nMINECARTS",
		x = 0,
		y = -200,
	}
	tween(1,titleSprite,{y = 50},"outBack")
	
	touchText = {
		text = "Touch to play",
		x = -800,
		y = 450
	}
	tween(0.5,{},{},"linear",function() 
		tween(0.8,touchText,{x = 0},"outBack") 
	end)

	playButton = {
		text = "Start Game",
		x = -800,
		y = 400,
	}
	tween(0.5,{},{},"linear",function() 
		tween(0.8,playButton,{x = 0},"outBack")
	end)

	editorButton = {
		text = "Edit Level",
		x = -800,
		y = 450,
	}
	tween(0.6,{},{},"linear",function() 
		tween(0.8,editorButton,{x = 0},"outBack")
	end)
	
	quitButton = {
		text = "Quit",
		x = -800,
		y = 500,
	}
	tween(0.7,{},{},"linear",function() 
		tween(0.8,quitButton,{x = 0},"outBack")
	end)
end

function s.update(dt)
	tween.update(dt)
end

function s.draw()
	love.graphics.printf(titleSprite.text,titleSprite.x,titleSprite.y,love.graphics.getWidth()/10,"center",0,10)
	if isMobile then
		love.graphics.printf(touchText.text,touchText.x,touchText.y,love.graphics.getWidth()/3,"center",0,3)
	else
		love.graphics.printf(playButton.text,playButton.x,playButton.y,love.graphics.getWidth()/3,"center",0,3)
		love.graphics.printf(editorButton.text,editorButton.x,editorButton.y,love.graphics.getWidth()/3,"center",0,3)
		love.graphics.printf(quitButton.text,quitButton.x,quitButton.y,love.graphics.getWidth()/3,"center",0,3)
	end
	--[[if isMobile then
		love.graphics.printf("CRAZY\nMINECARTS",0,50,love.graphics.getWidth()/10,"center",0,10)
		love.graphics.printf("Touch to play",0,450,love.graphics.getWidth()/3,"center",0,3)
	else
		love.graphics.printf("CRAZY\nMINECARTS",0,150,love.graphics.getWidth()/10,"center",0,10)
		love.graphics.printf("(P)lay the game,\n(E)dit a level or\n(Q)uit",0,450,love.graphics.getWidth()/3,"center",0,3)
	end]]
end

function s.keypressed(k)
	if k == "p" or k == "P" then
		Scene.Load("levelTransition")
	end
	if k == "e" or k == "E" then
		Scene.Load("editor")
	end
	if isMobile then
		if k == "escape" then love.event.quit() end
	else
		if k == "q" or k == "Q" then love.event.quit() end
	end
end

function s.touchpressed(...)
    Scene.Load("levelTransition")
end

return s