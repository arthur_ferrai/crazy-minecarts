require "libs.scene"
require "libs.camera"
require "libs.loveframes"
local s = {}
local modes = {
	"insert",
	"edit",
	"view",
}
local subModes = {
	"point",
	"deco",
	"ore",
	"turbo",
	"slow",
}
-- modes are: insert, edit, view
local currentMode = "insert"
-- submodes are: point,deco,ore,turbo,slow
local subMode = "point"

local selectedGroup = 0
local selectedPoint = 1

local pointsGroups = {}
local curves = {}
local cam = nil
local items = {}
local addItemMenu
local nextItemPos = {x = 0, y = 0}

local limits = {l = 0, r = 0, u = 0, d = 0}

-- loveframes windows (needed to unload correctly when unloading scene)
local loadFrame
local frame
local subModeList
local saveFrame
local infoFrame

-- limits control
local upperLimitBox
local leftLimitBox
local rightLimitBox
local lowerLimitBox

local function clamp(val, min, max)
	return (val < min) and min or ((val > max) and max or val)
end

local function addGroup()
	table.insert(pointsGroups,{})
	selectedGroup = #pointsGroups
end

local function saveLevel(name)
	if name == "" then return false end
	love.filesystem.createDirectory("rails")
	local f = love.filesystem.newFile("rails/"..name..".lua")
	f:open("w")
	f:write("return {\r\n")
	f:write(" pointsGroups = {\r\n")
	for _,points in pairs(pointsGroups) do
		f:write("  {\r\n")
		for i=1, #points, 2 do
			f:write(string.format("   %f,%f,\r\n",points[i],points[i+1]))
		end
		f:write("  },\r\n")
	end
	f:write(" },\r\n") --pointsGroups
	f:write(" items = {\r\n")
	for _,item in pairs(items) do
		f:write("  {")
		for k,v in pairs(item) do --items are named fields
			if type(v) == "string" then
				f:write(k.."=\""..v.."\", ")
			else
				f:write(k.."="..v..", ")
			end
		end
		f:write("},\r\n")
	end
	f:write(" },\r\n") -- items
	f:write(string.format(" limits={l=%d,r=%d,u=%d,d=%d},\r\n",limits.l,limits.r,limits.u,limits.d))
	f:write("}") --return
	f:close()
	return true
end

local function loadLevel(name)
	local chunk = love.filesystem.load("rails/"..name..".lua")
	if not chunk then addGroup() return end
	pointsGroups = chunk().pointsGroups or {}
	items = chunk().items or {}
	limits = chunk().limits or {l=0,r=0,u=0,d=0}
	for _,points in ipairs(pointsGroups) do
		table.insert(curves,love.math.newBezierCurve(points))
	end
	selectedGroup = 1
	upperLimitBox:SetValue(limits.u)
	leftLimitBox:SetValue(limits.l)
	rightLimitBox:SetValue(limits.r)
	lowerLimitBox:SetValue(limits.d)
end

function s.load()
	currentMode = "insert"
	subMode = "point"
	selectedPoint = 1
	pointsGroups = {}
	curves = {}
	selectedGroup = 0
	items = {}
	limits = {l = 0, r = 0, u = 0, d = 0}

	cam = Camera()
	cam.newLayer(1, 
		function() 
			local r,g,b,a = love.graphics.getColor()
			local w = love.graphics.getLineWidth()
			love.graphics.setLineWidth(1)
			love.graphics.setLineStyle("smooth")

			-- draw items
			love.graphics.setColor(255,0,0,255)
			for k,item in ipairs(items) do
				if currentMode == "edit" and subMode ~= "point" and k == selectedPoint then --paint selected only if editing items
					love.graphics.circle("fill",item.x, item.y,25)
				else
					love.graphics.circle("line",item.x, item.y,25)
				end
				love.graphics.print(item.name,item.x, item.y + 25)
			end

			-- draw control points
			for k,v in pairs(pointsGroups) do
				if subMode == "point" and k == selectedGroup then
					love.graphics.setColor(0,255,0,255)
				else
					love.graphics.setColor(0,255,0,64)
				end
				for i = 1,#v,2 do
					if currentMode == "edit" and subMode == "point" and k == selectedGroup and i/2+0.5 == selectedPoint then -- paint selected only if editing curve
						love.graphics.circle("fill",v[i],v[i+1],20,25)
					else
						love.graphics.circle("line",v[i],v[i+1],20,25)
					end
					love.graphics.print((i/2) + 0.5,v[i],v[i+1])
				end
				if #v > 2 then
					love.graphics.line(v)
				end
			end

			-- link control points
			love.graphics.setLineWidth(5)
			love.graphics.setColor(255,255,255,255)
			-- draw bezier curve
			love.graphics.setLineStyle("rough")
			for _,v in pairs(curves) do
				love.graphics.line(v:render())
			end

			love.graphics.setColor(255,0,0,255)

			-- draw level limits
			love.graphics.rectangle("line",limits.l, limits.u,limits.r - limits.l ,limits.d - limits.u)

			-- restore draw parameters
			love.graphics.setLineWidth(w)
			love.graphics.setColor(r,g,b,a)
		end)
	
	-- loveframes state initialization
	loveframes.SetState("Load")

	-- create save window
	saveFrame = loveframes.Create("frame"):SetName("Save Level"):ShowCloseButton(false):SetWidth(350):SetHeight(130):Center():SetState("Save")
	loveframes.Create("text",saveFrame):SetText("Level name:"):SetY(35):CenterX()
	local savePath = loveframes.Create("textinput",saveFrame):SetText(""):SetMultiline(false):SetY(55):SetWidth(320):CenterX()
	local saveButton = 	loveframes.Create("button",saveFrame):SetText("Save"):SetPos(15,90):SetWidth(80)
	local cancelButton = loveframes.Create("button",saveFrame):SetText("Cancel"):SetPos(255,90):SetWidth(80)
	local dontSaveButton = loveframes.Create("button",saveFrame):SetText("Don't Save"):SetY(90):CenterX():SetWidth(80)
	saveButton.OnClick = function( ... )
		if saveLevel(savePath:GetText()) then
			Scene.Load("menu")
		end
	end
	dontSaveButton.OnClick = function( ... )
		Scene.Load("menu")
	end
	cancelButton.OnClick = function( ... )
		loveframes.SetState("none")
	end

	--create load level window
	loadFrame = loveframes.Create("frame"):SetName("Load Level"):ShowCloseButton(false):SetState("Load"):SetWidth(215):SetHeight(90):Center()
	local levelsList = loveframes.Create("multichoice",loadFrame):SetWidth(340):SetPos(5,30):SetWidth(205)
	local files = love.filesystem.getDirectoryItems("rails")
	for _,f in ipairs(files) do
		levelsList:AddChoice(f:sub(1,-5))
		levelsList:SetChoice(f:sub(1,-5))
	end
	local loadLevelButton = loveframes.Create("button", loadFrame):SetText("Load Level"):SetPos(5,60):SetWidth(100)
	loadLevelButton.OnClick = function( ... )
		loadLevel(levelsList:GetChoice())
		savePath:SetText(levelsList:GetChoice())
		loveframes.SetState("none")
	end
	local newLevelButton = loveframes.Create("button", loadFrame):SetText("Create New"):SetPos(110,60):SetWidth(100)
	newLevelButton.OnClick = function( ... )
		addGroup()
		savePath:SetText("")
		loveframes.SetState("none")
	end
	
	-- create mode selector window
	frame = loveframes.Create("frame"):SetName("Mode Selector"):ShowCloseButton(false):SetWidth(140):SetHeight(210)
	loveframes.Create("text", frame):SetText("Mode:"):SetPos(5,35)
	loveframes.Create("text", frame):SetText("SubMode:"):SetPos(5,65)
	local modeList = loveframes.Create("multichoice",frame):SetPos(75,30):SetWidth(60)
	for _,v in ipairs(modes) do
		modeList:AddChoice(v)
	end
	modeList:SetChoice(modes[1])
	modeList.OnChoiceSelected = function (o,c)
		currentMode = c
	end
	loveframes.Create("text",frame):SetText("Level Limits:"):SetY(100):CenterX()
	upperLimitBox = loveframes.Create("numberbox",frame):SetWidth(60):SetY(120):CenterX():SetMin(-999999):SetMax(999999)
	upperLimitBox.OnValueChanged = function(obj,val)
		limits.u = val
	end
	leftLimitBox = loveframes.Create("numberbox",frame):SetWidth(60):SetY(150):SetX(5):SetMin(-999999):SetMax(999999)
	leftLimitBox.OnValueChanged = function(obj,val)
		limits.l = val
	end
	rightLimitBox = loveframes.Create("numberbox",frame):SetWidth(60):SetY(150):SetX(75):SetMin(-999999):SetMax(999999)
	rightLimitBox.OnValueChanged = function(obj,val)
		limits.r = val
	end
	lowerLimitBox = loveframes.Create("numberbox",frame):SetWidth(60):SetY(180):CenterX():SetMin(-999999):SetMax(999999)
	lowerLimitBox.OnValueChanged = function(obj,val)
		limits.d = val
	end
	
	-- create item selector menu
	subModeList = loveframes.Create("multichoice",frame):SetPos(75,60):SetWidth(60)
	subModeList:AddChoice("point")
	subModeList:AddChoice("item")
	subModeList:SetChoice("point")
	subModeList.OnChoiceSelected = function(o,c)
		subMode = c
	end

	-- create "add item" menu
	addItemMenu = loveframes.Create("menu"):SetVisible(false)
	addItemMenu:AddOption("Ore",  false,function() table.insert(items,{name = "ore",   x = nextItemPos.x, y = nextItemPos.y}) loveframes.SetState("none") end)
	addItemMenu:AddOption("Turbo",false,function() table.insert(items,{name = "turbo", x = nextItemPos.x, y = nextItemPos.y}) loveframes.SetState("none") end)
	addItemMenu:AddOption("Slow", false,function() table.insert(items,{name = "slow",  x = nextItemPos.x, y = nextItemPos.y}) loveframes.SetState("none") end)
	addItemMenu:AddOption("Checkpoint/Respawn", false,function() table.insert(items,{name = "checkpoint",  x = nextItemPos.x, y = nextItemPos.y}) loveframes.SetState("none") end)


	-- create info window
	infoFrame = loveframes.Create("frame"):SetName("Instructions"):SetHeight(120):CenterX()
	loveframes.Create("text",infoFrame):SetText("Arrows move camera"):SetPos(5,30):CenterX()
	loveframes.Create("text",infoFrame):SetText("+/- zooms in/out"):SetPos(5,50):CenterX()
	loveframes.Create("text",infoFrame):SetText("PgUp/PgDn selects next/prev point/group"):SetPos(5,70):CenterX()
	loveframes.Create("text",infoFrame):SetText("Esc to exit/save level"):SetPos(5,90):CenterX()
end

function s.unload()
	cam = nil
	curves = nil
	pointsGroups = nil
	currentMode = nil
	loadFrame:Remove()
	frame:Remove()
	subModeList:Remove()
	saveFrame:Remove()
	infoFrame:Remove()
end



function s.mousepressed(x,y,b)
	loveframes.mousepressed(x,y,b)
	if loveframes.util.GetHover() then return end
	local cx,cy = cam.mousePosition()
	if currentMode == "insert" then
		if subMode == "point" then
			if b == "l" then
				table.insert(pointsGroups[selectedGroup],cx)
				table.insert(pointsGroups[selectedGroup],cy)
				if #pointsGroups[selectedGroup] > 2 then
					curves[selectedGroup] = love.math.newBezierCurve(pointsGroups[selectedGroup])
				end
			end
		else
			if b == "l" and not addItemMenu:GetVisible() then
				nextItemPos.x = cx
				nextItemPos.y = cy
				addItemMenu:SetPos(x + 1,y + 1):SetVisible(true)
			end
		end
	elseif currentMode == "edit" then
		
	end
end

function s.mousereleased(x,y,b)
	loveframes.mousereleased(x,y,b)
end

function s.draw()
	cam.draw()
	loveframes.draw()
end

function s.keypressed(key, isRepeat)
	if key == "escape" and loveframes.GetState() == "none" then loveframes.SetState("Save") end
	if key == "+" or key == "kp+" then cam.scale(0.9) end
	if key == "-" or key == "kp-" then cam.scale(1.1) end
	
	
	-- insert mode: add points at the end
	if currentMode == "insert" and subMode == "point" then
		if key == "return" then
			addGroup()
		end
		if key == "pagedown" then
			selectedGroup = selectedGroup - 1
		end
		if key == "pageup" then
			selectedGroup = selectedGroup + 1
		end
		if selectedGroup > #pointsGroups then selectedGroup = 1
		elseif selectedGroup < 1 then selectedGroup = #pointsGroups end

	-- edit mode: move/delete existing points
	elseif currentMode == "edit" then
		if key == "pagedown" then
			selectedPoint = selectedPoint - 1
		end
		if key == "pageup" then
			selectedPoint = selectedPoint + 1
		end
		if key == "delete" then
			if subMode == "point" then
				table.remove(pointsGroups[selectedGroup],selectedPoint*2 - 1)
				table.remove(pointsGroups[selectedGroup],selectedPoint*2 - 1)
				if #pointsGroups[selectedGroup] > 0 then
					if #pointsGroups[selectedGroup] > 2 then
						curves[selectedGroup] = love.math.newBezierCurve(pointsGroups[selectedGroup])
					else
						curves[selectedGroup] = nil
					end
				else
					table.remove(pointsGroups,selectedGroup)
					table.remove(curves,selectedGroup)
					selectedGroup = clamp(selectedGroup,1,#pointsGroups)
				end
			else
				table.remove(items,selectedPoint)
			end
		end
		if subMode == "point" then
			if selectedPoint > #pointsGroups[selectedGroup]/2 then selectedPoint = 1
			elseif selectedPoint < 1 then selectedPoint = #pointsGroups[selectedGroup]/2 end
		else
			if selectedPoint > #items then selectedPoint = 1
			elseif selectedPoint < 1 then selectedPoint = #items end
		end
	-- view mode: navigate camera
	elseif currentMode == "view" then
	end
	loveframes.keypressed(key, isRepeat)
end

function s.keyreleased(k)
	loveframes.keyreleased(k)
end

function s.update(dt)
	-- drag selected point
	if love.mouse.isDown("l") and currentMode == "edit" and not loveframes.util.GetHover() then
		local x,y = cam.mousePosition()
		if subMode == "point" then
			pointsGroups[selectedGroup][selectedPoint*2 - 1] = x
			pointsGroups[selectedGroup][selectedPoint*2] = y
			curves[selectedGroup] = love.math.newBezierCurve(pointsGroups[selectedGroup])
		else
			items[selectedPoint].x = x
			items[selectedPoint].y = y
		end
	end

	-- move camera using arrows
	local moveX, moveY = 0,0
	if love.keyboard.isDown("left") then
		moveX = -10
	elseif love.keyboard.isDown("right") then
		moveX = 10
	end
	if love.keyboard.isDown("up") then
		moveY = -10
	elseif love.keyboard.isDown("down") then
		moveY = 10
	end
	cam.move(moveX,moveY)

	loveframes.update(dt)
end

function s.textinput(text)
	loveframes.textinput(text)
end

return s