Item = {}
local mt = {__index = Item}

local imgList = {}

function Item.new(world,name,x,y)
    local item = {}
    if not imgList[name] then
        imgList[name] = love.graphics.newImage("img/"..name..".png")
    end
    local img = imgList[name]
    local body = love.physics.newBody(world,x,y)
    local fixture = love.physics.newFixture(body,love.physics.newCircleShape(img:getWidth()/2, img:getHeight()/2,img:getWidth()/2),x,y)
    fixture:setUserData("item/"..name)
    fixture:setSensor(true)
    item.draw = function ()
        love.graphics.draw(img,x,y)
    end

    item.setPhysicsCategory = function(cat)
        fixture:setCategory(cat)
    end

    item.setPhysicsMask = function(...)
        fixture:setMask(...)
    end

    item.getFixture = function()
        return fixture
    end

    item.getPos = function()
        return x, y
    end

    setmetatable(item,mt)
    return item
end