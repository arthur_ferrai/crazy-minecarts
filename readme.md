# Crazy Minecarts

>Mike was walking happily when he suddenly drops through a hole directly into a minecart. Now, you need to help Mike get out of the mine, not letting him fall out the minecart.

This game will be the February's One Game a Month entry

## About the game

### Ambiance

The game passes on an abandoned mine, with torches illuminating the path, and rails.

### Gameplay

It will use 2D, platform-like mechanics, and Box2D physics

The player controls the minecart, which will have Mike inside it. The player can only accelerate left and right (can't spin the minecart, it will be aligned to the rail).

The levels will be finite (not autogen), and at the end of the last one, Mike will drop off the mine, close to the place he was.

There will be items to help the player:

* ores: add score
* turbo: twice as fast as normal max speed
* glue: don't let mike drop off the cart

Checkpoints will have an upper rail with another stationary cart and a hole-like light. If Mike falls off the cart, another Mike will drop from the hole to the cart.

Every time Mike falls off the cart, he loses collected ores. 

At the end of stage, the collected ores are counted, and added to score of elapsed time (less time = more score).


### TODO

* Box2D prototype - Done.
* Rail draw (possibly using bezier curves) - Done.
* Make collectable items - Done (lacks final sprites).
* Level transitions - Done
* cart sprite
* Mike sprite (probably with separated body parts to put in a ragdoll)
* Background
* Menu art
* Story art