extern number factor;
vec4 effect(vec4 color, Image img, vec2 uv, vec2 pixel_coords){
    number delta = 1.0/factor;
    vec2 coord = vec2(delta*ceil(uv.x/delta),
                      delta*ceil(uv.y/delta));
    return Texel(img, coord);
}